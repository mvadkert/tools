# Swiss Army Knife

`knife` is a CLI tool implementing various useful commands which were too small to deserve an own project.

## Command `containers registry remove-tags`

Remove tags from a remote Docker registry.
